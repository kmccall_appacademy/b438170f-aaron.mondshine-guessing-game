# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  random_number = create_random_number[-1]
  guesses = 0
  guess = nil


  puts "Guess a Number"
  loop do
    guess = gets.chomp.to_i
    guesses += 1
    #puts "Incorrect Entry, Try Again" if !valid_number(guess)
    next if !valid_number(guess)
    break if guess == random_number
    print guess
    puts low_or_high(guess, random_number)

  end

  puts "#{guess} is Correct! It took you #{guesses} guessses."
end

def create_random_number
  (1..100).to_a.shuffle
end

def low_or_high(guess, random_number)
  if guess < random_number
    " too low, guess again"
  else
    " too high, guess again"
  end
end

def valid_number(guess)
  guess >= 0 && guess <= 100
end
